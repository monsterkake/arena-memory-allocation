#include <iostream>
#include <string>
#include <inttypes.h>
#include <vector>
#include <memory>
#include <typeinfo>


class Parent {
protected:
	std::string m_name;
public:
	Parent(std::string name) : m_name(name) {}
	virtual std::string getName() { return m_name; }

};

class Child : public Parent
{
private:
	int m_id;
public:
	Child(std::string name, int id) : Parent(name), m_id(id) {}
	virtual std::string getName() override { return m_name + std::to_string(m_id); }
	int getId() { return m_id; }

};

class Arena
{
private:
	uint8_t* m_memoryPointer;
	uint8_t* m_memoryEndPointer;
	uint8_t* m_currentAddress;
	uint64_t m_size;

public:
	Arena(size_t size) : m_size(size) {
		m_memoryPointer = new uint8_t[size];
		m_memoryEndPointer = m_memoryPointer + size;
		m_currentAddress = m_memoryPointer;

	}
	~Arena() {
		delete[] m_memoryPointer;
	}


	void* allocate(size_t size) {
		if (m_currentAddress + size > m_memoryEndPointer) 
			throw std::runtime_error("Arena m_currentAddress out of bounds");

		uint8_t* temp = m_currentAddress;
		m_currentAddress += size;
		return temp;
		
	}

};



int main() {
	Parent parent("name0");
	Child child("name1", 0);
	Arena arena(2048);
	std::vector<Parent*> parentArray;

	std::cout << sizeof Parent << std::endl;

	
	for (int i = 0; i < 10; i++) {
		Parent* ptr = new (arena.allocate(sizeof Parent)) Parent("parent " + std::to_string(i));

		parentArray.push_back(ptr);

	}
	for (int i = 0; i < 10; i++) {
		Parent* ptr = new (arena.allocate(sizeof Child)) Child("Child ", i);

		parentArray.push_back(ptr);

	}

	for (auto ptr : parentArray) {

		std::cout << ptr->getName() << std::endl;
	}

	//std::cout << parent.getName() << std::endl;
	//std::cout << child.getName() << " " << child.getId() << std::endl;
	system("pause");
	return 0;
}